/// <reference types="react" />
import { Configuration as FaviconsConfiguration } from 'favicons';
interface Options {
    /**
     * The source image
     */
    inputFile: string;
    /**
     * Directory where the image files will be written
     */
    outputDir?: string;
    /**
     * Configuration for `favicons`, see https://www.npmjs.com/package/favicons#nodejs
     */
    configuration?: FaviconsConfiguration;
}
interface HookOptions {
    meta: {
        faviconsElements?: JSX.Element[];
    };
}
interface ReactStaticConfig {
    paths: {
        ASSETS: string;
    };
}
declare const _default: (options: Options) => {
    afterGetConfig: (config: ReactStaticConfig) => ReactStaticConfig;
    beforeRenderToHtml: (element: JSX.Element, { meta }: HookOptions) => Promise<JSX.Element>;
    headElements: (elements: JSX.Element[], { meta }: HookOptions) => Promise<(JSX.Element | JSX.Element[] | undefined)[]>;
} | undefined;
export default _default;
